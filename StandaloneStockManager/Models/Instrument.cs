﻿using System;

namespace StandaloneStockManager.Models
{
    [Serializable]
    public class Instrument : BaseModel
    {
        protected TypeOfInstrument _typeOfInstrument;

        public TypeOfInstrument TypeOfInstrument {
            get => _typeOfInstrument;
            set {
                _typeOfInstrument = value;
                OnPropertyChanged("TypeOfInstrument");
            }
        }

        string _name;

        public string Name {
            get => _name;
            set {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public Instrument(TypeOfInstrument typeOfInstrument, string name)
        {
            this._typeOfInstrument = typeOfInstrument;
            this._name = name;
        }

        public Instrument(Instrument newInstrument)
        {
            _typeOfInstrument = newInstrument._typeOfInstrument;
            _name = newInstrument._name;
        }

    }
}
