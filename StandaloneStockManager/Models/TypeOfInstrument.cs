﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using StandaloneStockManager.ViewModels;

namespace StandaloneStockManager.Models
{
    [Serializable]
    public class TypeOfInstrument : BaseModel
    {
        [DefaultValue("Akcje")]
        string _name;
        public string Name {
            get => _name;
            set {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        [DefaultValue(0.0019f)]
        float _commision;
        public float Commision {
            get => _commision;
            set {
                _commision = value;
                OnPropertyChanged("Commision");
            }

        }

        [DefaultValue(3.00)]
        decimal _minimalCommision;
        public decimal MinimalCommision {
            get => _minimalCommision;
            set {
                _minimalCommision = value;
                OnPropertyChanged("MinimalCommision");
            }
        }

        public TypeOfInstrument(string name, float commsion, decimal minCommision)
        {
            Name = name;
            Commision = commsion;
            MinimalCommision = minCommision;
        }
    }
}
