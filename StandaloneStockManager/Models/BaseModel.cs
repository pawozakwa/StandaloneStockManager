﻿using System;
using System.ComponentModel;

namespace StandaloneStockManager.Models
{
    [Serializable]
    public class BaseModel : INotifyPropertyChanged
    {
        [field:NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(params string[] propertiesNames)
        {
            if (PropertyChanged == null) return;
            foreach (var propertyName in propertiesNames)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
