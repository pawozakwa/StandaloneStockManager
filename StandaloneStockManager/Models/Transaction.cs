﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Controls;
using System.Windows.Media;
using StandaloneStockManager.DAL;
using StandaloneStockManager.Helpers;
using StandaloneStockManager.ViewModels;

namespace StandaloneStockManager.Models
{
    public enum TransactionType
    {
        Buy,
        Sell,
        Hold
    }

    [Serializable]
    public class Transaction : Instrument
    {
        readonly bool _representOwnedInstrument = false;

        #region properties saved as data

        TransactionType _transactionType;

        public TransactionType TransactionType {
            get => _transactionType;
            set {
                _transactionType = value;
                if (_representOwnedInstrument)
                    _transactionType = TransactionType.Hold;
                else if (_transactionType == TransactionType.Hold)
                    _transactionType = TransactionType.Buy;

                OnPropertyChanged("TransactionType");
            }
        }


        [DefaultValue(1.0f)]
        float _count;

        public float Count {
            get => _count;
            set {
                _count = value;
                MarketPrice = _marketPrice;
                OnPropertyChanged("Count");
                OnPropertyChanged("MarketPrice");
            }
        }

        /// <summary>
        /// Average price of one instrument 
        /// </summary>
        [DefaultValue(0)]
        decimal _marketPrice;

        public decimal MarketPrice {
            get => _marketPrice;
            //decimal.Round(_marketPrice, 2, MidpointRounding.AwayFromZero);
            set {
                _marketPrice = value;
                _realPrice = MoneyRound(IncludeCommisionToOneItem(value) * (decimal)_count);
                OnPropertyChanged("MarketPrice");
                OnPropertyChanged("RealPrice");
                OnPropertyChanged("RealPriceOfOneItem");
                OnPropertyChanged("ZeroPrice");
            }
        }

        //private string _currency;
        //public string Currency {
        //    get => _currency;
        //    set
        //    {
        //        _currency = value;
        //        OnPropertyChanged("Currency");
        //    }
        //}

        #region transaction time
        private DateTime _time;
        public DateTime Time {
            set {
                _time = value;
                OnPropertyChanged("Time");
            }
        }

        public string DateAsString => _time.ToShortDateString();

        public string TimeAsString => _time.ToShortTimeString();
        #endregion
        #endregion

        #region dynamic calculated properties

        /// <summary>
        /// real cost of one item, contain only buy commision
        /// </summary>
        public decimal RealPriceOfOneItem {
            get
            {
                var oneItemPrice = _realPrice / (decimal) _count;
                return MoneyRound(oneItemPrice);
            }
        }

        public decimal _realPrice;
        /// <summary>
        /// Real cost of whole transaction, contain with added cost when buy, and removed commision when sell
        /// </summary>
        public decimal RealPrice {
            get => _realPrice;
            set
            {
                _marketPrice = MoneyRound(RemoveCommision(value) / (decimal)_count);
                _realPrice = value;
                OnPropertyChanged("MarketPrice");
                OnPropertyChanged("RealPrice");
                OnPropertyChanged("RealPriceOfOneItem");
                OnPropertyChanged("ZeroPrice");
            }
        }

        #region helpers
        private decimal IncludeCommision(decimal value)
        {
            var commision = (decimal)((float)value * base.TypeOfInstrument.Commision);
            commision = commision.Clamp(base.TypeOfInstrument.MinimalCommision);
            var result = 0.00m;
            switch (_transactionType)
            {
                case TransactionType.Buy:
                    result = value + commision;
                    break;
                case TransactionType.Sell:
                    result = value - commision;
                    break;
            }
            return MoneyRound(result);
        }

        private decimal RemoveCommision(decimal value)
        {
            var commision = (decimal)((float)value * base.TypeOfInstrument.Commision);
            commision = commision.Clamp(base.TypeOfInstrument.MinimalCommision);
            var result = default(decimal);
            switch (_transactionType)
            {
                case TransactionType.Buy:
                    result = value / (decimal)(1.00f + base.TypeOfInstrument.Commision);
                    break;
                case TransactionType.Sell:
                    result = value / (decimal)(1.00f - base.TypeOfInstrument.Commision);
                    break;
            }
            return result;
        }

        private decimal IncludeCommisionToOneItem(decimal value)
        {
            var commision = (decimal)((float)value * base.TypeOfInstrument.Commision);
            var result = 0.00m;
            switch (_transactionType)
            {
                case TransactionType.Buy:
                    result = value + commision;
                    break;
                case TransactionType.Sell:
                    result = value - commision;
                    break;
            }
            return decimal.Round(result, 2, MidpointRounding.AwayFromZero);
        }

        private static decimal MoneyRound(decimal zeroPrice)
        {
            return decimal.Round(zeroPrice, 2, MidpointRounding.AwayFromZero);
        }
        #endregion

        /// <summary>
        /// MarketPrice level which the sale will not bring profit or loss, contain commision of buy, and sell
        /// </summary>
        public decimal ZeroPrice {
            get
            {
                if (TransactionType == TransactionType.Sell)
                    return 0.0m;

                var product = RealPriceOfOneItem;
                var commision = (decimal)((float)product * base.TypeOfInstrument.Commision);
                if (_transactionType == TransactionType.Buy)
                    commision *= 2.00m;
                var zeroPrice = product + commision;
                return MoneyRound(zeroPrice);
            }
        }



        private decimal _actualPrice = default(decimal);
        public decimal ActualPrice {
            get {
                if (_actualPrice == default(decimal))
                {
                    _actualPrice = BankierDataProvider.LastDecimalStockPrice(Name);
                    OnPropertyChanged("ActualPrice");
                    OnPropertyChanged("Profit");
                }

                return _actualPrice;
            }
            private set { }
        }

        //public TextBlock Profit {
        //    get
        //    {
        //        float value = (Count * (float)(ActualPrice - ZeroPrice));
        //        var profitText = new TextBlock();
        //        profitText.Text = value.ToString("c2");

        //        if (value > 0) profitText.Foreground = Brushes.Chartreuse;
        //        if (value < 0) profitText.Foreground = Brushes.Red;
        //        if (value == 0) profitText.Foreground = Brushes.Bisque;

        //        return profitText;
        //    }
        //}

        public float Profit {
            get {
                if (TransactionType == TransactionType.Sell)
                    return default(float);
                return (Count * (float)(ActualPrice - ZeroPrice));
            }
        }


        #endregion

        public Transaction(bool isOwned, Instrument instrument, TransactionType transType, float count, decimal averageMarketPrice) :
            base(instrument)
        {
            this._representOwnedInstrument = isOwned;
            this._count = count;
            this._transactionType = transType;
            this._marketPrice = averageMarketPrice;
            Time = DateTime.Now;
        }

 
        public Transaction GetInstrumentToOwn()
        {
            return new Transaction(true, this, TransactionType.Hold, _count, MarketPrice);
        }

        public decimal GetTransactionTypeModifier()
        {
            var transactionTypeModifier = 1m;
            switch (_transactionType)
            {
                case TransactionType.Buy:
                    transactionTypeModifier = 1m;
                    break;
                case TransactionType.Sell:
                    transactionTypeModifier = -1m;
                    break;
                case TransactionType.Hold:
                    throw new ArgumentOutOfRangeException();
            }
            return transactionTypeModifier;
        }
    }
}
