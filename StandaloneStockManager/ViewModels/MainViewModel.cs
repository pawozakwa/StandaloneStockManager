﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using StandaloneStockManager.Models;
using System.Linq;

namespace StandaloneStockManager.ViewModels
{
    [Serializable]
    public class MainViewModel
    {
        #region singleton
        static MainViewModel _instance;

        public static MainViewModel Instance {
            get => _instance;
            private set { }
        }

        public static MainViewModel GenerateTemplate()
        {
            return new MainViewModel();
        }
        #endregion


        public ObservableCollection<TypeOfInstrument> InstrumentsTypes { get; set; }

        public ObservableCollection<Transaction> Transactions { get; set; }

        public ObservableCollection<Instrument> InstrumentsPrototypes { get; set; }

        public Dictionary<string, TypeOfInstrument> InstrumentsTypesDictionary { get; set; }

        public Dictionary<string, Instrument> InstrumentsPrototypesDictionary { get; set; }

        public void AddInstrumentType(TypeOfInstrument newInstrument)
        {
            InstrumentsTypes.Add(newInstrument);
            RefreshInstrumentsTypesDictionary();
        }

        public void AddTransaction(Transaction newTransaction)
        {
            Transactions.Add(newTransaction);
        }


        public Dictionary<string, string> Currencies { get; set; }

        [DefaultValue("zł")]
        public static string ChoosenCurrencySymbol { get; set; }

        public MainViewModel()
        {
            _instance = this;
            InitWithDefaultValues();
        }

        public List<Transaction> OwnedInstruments {
            get {
                var listOfOwnedInstruments = new List<Transaction>();
                
                RefreshInstrumentsPrototypesDictionary();

                foreach (var name in OwnedInstrumentsNames)
                {
                    GetInstrumentPossesion(name, ref listOfOwnedInstruments);
                }
                
                return listOfOwnedInstruments;
            }
        }

        public HashSet<string> OwnedInstrumentsNames {
            get {
                var ownedNames = new HashSet<string>();
                foreach (var transaction in Transactions)
                {
                    var transName = transaction.Name;
                    if (!ownedNames.Contains(transName))
                    {
                        ownedNames.Add(transName);
                    }
                }
                return ownedNames;
            }
        }
        
        private void GetInstrumentPossesion(string name, ref List<Transaction> listOfInstruments)
        {
            var transactionsWithInstrumentName = from transaction in Transactions
                                                 where transaction.Name == name
                                                 select transaction;

            var totalCost = 0m;
            var count = 0f; // final count

            var ownedInstrument = Transactions.First(i => i.Name == name).GetInstrumentToOwn();

            SumAllTransactionsOnInstrument(transactionsWithInstrumentName, ref totalCost, ref count);

            var decCount = (decimal) count;
            var averagePrice = totalCost / decCount;

            ownedInstrument.MarketPrice = averagePrice;
            ownedInstrument.Count = count;
            listOfInstruments.Add(ownedInstrument);
        }

        private static void SumAllTransactionsOnInstrument(IEnumerable<Transaction> transactionsWithInstrumentName, ref decimal cost,
            ref float count)
        {
            foreach (var transaction in transactionsWithInstrumentName)
            {
                var transactionTypeModifier = transaction.GetTransactionTypeModifier();

                count += transaction.Count * (float)transactionTypeModifier;
                cost += transaction.RealPriceOfOneItem * transactionTypeModifier * (decimal)count;
            }
        }

        private void InitWithDefaultValues()
        {
            Currencies = new Dictionary<string, string>
            {
                {"PLN", "zł"},
                {"USD", "$"},
                {"EUR", "€"}
            };

            InstrumentsTypes = new ObservableCollection<TypeOfInstrument>
            {
                //new TypeOfInstrument("Akcje", 0.0039f, 3.0m),
                //new TypeOfInstrument("Fundusze", 0, 0)
            };

            RefreshInstrumentsTypesDictionary();

            InstrumentsPrototypes = new ObservableCollection<Instrument>
            {
                //new Instrument(InstrumentsTypesDictionary["Akcje"], "CDProjekt"),
                //new Instrument(InstrumentsTypesDictionary["Akcje"], "11BIT")
            };

            RefreshInstrumentsPrototypesDictionary();

            Transactions = new ObservableCollection<Transaction>
            {
                //new Transaction(false, InstrumentsPrototypesDictionary["11BIT"], TransactionType.Buy , 30, 198.0m),
                //new Transaction(false, InstrumentsPrototypesDictionary["CDProjekt"], TransactionType.Buy , 34, 86.05m),
            };

        }

        public void RefreshInstrumentsPrototypesDictionary()
        {
            if (InstrumentsPrototypesDictionary == null)
                InstrumentsPrototypesDictionary = new Dictionary<string, Instrument>();
            else
                InstrumentsPrototypesDictionary.Clear();

            foreach (var item in InstrumentsPrototypes)
            {
                InstrumentsPrototypesDictionary.Add(item.Name, item);
            }
        }

        public void RefreshInstrumentsTypesDictionary()
        {
            if (InstrumentsTypesDictionary == null)
                InstrumentsTypesDictionary = new Dictionary<string, TypeOfInstrument>();
            else
                InstrumentsTypesDictionary.Clear();

            foreach (var item in InstrumentsTypes)
            {
                InstrumentsTypesDictionary.Add(item.Name, item);
            }
        }
    }
}