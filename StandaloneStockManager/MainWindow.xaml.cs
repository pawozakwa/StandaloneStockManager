﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using StandaloneStockManager.DAL;
using StandaloneStockManager.ViewModels;
using StandaloneStockManager.Models;

namespace StandaloneStockManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainViewModel Mvm => (DataContext as MainViewModel);

        public MainWindow()
        {

            InitializeComponent();

            //DataContext = new MainViewModel();
            DataContext = LocalDataAccessLayer.LoadMainViewModel();

        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OwnedInstrumentsTab.IsSelected)
                RefreshOwnedInstruments();
        }


        #region Instrument types management
        private void SaveInstrumentButtonClick(object sender, RoutedEventArgs e)
        {
            var name = NewInstrumentName.Text;
            float commision;
            decimal minCommision;

            if (!ReadCommision(out commision)) return;

            if (!ReadMinCommision(out minCommision)) return;

            SaveNewInstrumentType(name, commision, minCommision);
        }

        private void SaveNewInstrumentType(string name, float commision, decimal minCommision)
        {
            Mvm?.AddInstrumentType(new TypeOfInstrument(name, commision, minCommision));
        }

        private void DeleteFromInstrumentList(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            Mvm.InstrumentsTypes.Remove((TypeOfInstrument)btn.DataContext);
        }

        /// <param name="commision"></param>
        /// <returns> false if entered commision isn't correct </returns>
        private bool ReadMinCommision(out decimal minCommision)
        {
            if (decimal.TryParse(NewInstrumentMinimalCommision.Text, out minCommision))
                return true;
            MessageBox.Show("Uncorrect minimal commision!");
            return false;
        }

        /// <param name="commision"></param>
        /// <returns> false if entered commision isn't correct </returns>
        private bool ReadCommision(out float commision)
        {
            try
            {
                commision = float.Parse(NewInstrumentCommision.Text, CultureInfo.InvariantCulture);

                commision /= 100.0f;
                if (commision > 0.01f)
                    commision /= 100.0f;

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Uncorrect commsion!");
                Console.WriteLine(e);
                throw;
            }
        }

        private void MoneyValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        #endregion

        #region Transaction management
        private void AddNewTransaction(object sender, RoutedEventArgs e)
        {
            var instrumentType = NewInstrumentType.SelectedValue as TypeOfInstrument;
            var instrumentName = NewInstrumentNameTextBox.Text;
            var instrumentPrototype = Mvm.InstrumentsPrototypesDictionary.ContainsKey(instrumentName)
                ? Mvm.InstrumentsPrototypesDictionary[instrumentName]
                : new Instrument(instrumentType, instrumentName);
            var t = new Transaction(false, instrumentPrototype, TransactionType.Buy, 1.00f, 2.00m);
            Mvm.Transactions.Add(t);
            Mvm.RefreshInstrumentsPrototypesDictionary();
            RefreshOwnedInstruments();
        }

        private void DeleteFromTransactionsList(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            Mvm.Transactions.Remove((Transaction)btn.DataContext);
            Mvm.RefreshInstrumentsPrototypesDictionary();
            RefreshOwnedInstruments();
        }

        private readonly string[] _disabledColumns = new string[]
        {
            "TypeOfInstrument",
            "Name"
        };
        private void TransactionsDataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (_disabledColumns.Contains(e.Column.Header.ToString()))
                e.Cancel = true;


        }


        private void TransactionsDataGrid_EndEditing(object sender, DataGridCellEditEndingEventArgs dataGridCellEditEndingEventArgs)
        {
            RefreshTransactions();
        }

        private void RefreshTransactions()
        {
            //Mvm.Transactions.

            //TransactionsDataGrid.ItemsSource = null;
            //TransactionsDataGrid.ItemsSource = Mvm.Transactions;
        }
        #endregion


        #region Settings management
        private void CurrencyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainViewModel.ChoosenCurrencySymbol = CurrencyComboBox.SelectedValuePath;
        }


        private void RefreshOwnedInstruments()
        {
            OwnedInstrumentsDataGrid.ItemsSource = null;
            OwnedInstrumentsDataGrid.ItemsSource = Mvm.OwnedInstruments;
        }

        #endregion

        #region saving data on exit

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataContext();
        }

        private void SaveDataContext() => LocalDataAccessLayer.SaveMainViewModel(Mvm);

        #endregion

        private void TestDataProvider(object sender, RoutedEventArgs e)
        {
            var result = BankierDataProvider.LastStockPrice("CDPROJEKT");
            MessageBox.Show(result, "Cena CDProjekt");
            decimal parsedResult;
            if (
                !Decimal.TryParse("30,00 zł", NumberStyles.Currency, CultureInfo.CurrentCulture, out parsedResult)
            )
                MessageBox.Show("SHIT", "Nie działa");
            MessageBox.Show(parsedResult.ToString(), "Sparsowana cena");
        }
    }
}
