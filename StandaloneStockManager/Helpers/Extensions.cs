﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StandaloneStockManager.Helpers
{
    public static class Extensions
    {
        public static decimal Clamp(this decimal val, decimal min = Decimal.MinValue, decimal max = Decimal.MaxValue)
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }

        public static float Clamp(this float val, float min = float.MinValue, float max = float.MaxValue)
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }
    }
}
