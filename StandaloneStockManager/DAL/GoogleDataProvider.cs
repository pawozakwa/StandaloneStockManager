﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace StandaloneStockManager.DAL
{
    class GoogleDataProvider
    {
        public static void testGoogleApi()
        {
            const string tickers = "AAPL,GOOG,GOOGL,YHOO,TSLA,INTC,AMZN,BIDU,ORCL,MSFT,ORCL,ATVI,NVDA,GME,LNKD,NFLX";

            string json;

            using (var web = new WebClient())
            {
                var url = $"https://stooq.pl/q/l/?s=11b&f=sd2t2ohlcv&h&e=csv";
                json = web.DownloadString(url);
            }

            //Google adds a comment before the json for some unknown reason, so we need to remove it
            json = json.Replace("//", "");

            var v = JArray.Parse(json);

            foreach (var i in v)
            {
                var ticker = i.SelectToken("t");
                var price = (decimal)i.SelectToken("l");

                Console.WriteLine($"{ticker} : {price}");
            }

            Console.WriteLine("Press any key to exit");
            Console.Read();
        }

    }
}
