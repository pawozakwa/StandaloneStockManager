﻿using StandaloneStockManager.ViewModels;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;

namespace StandaloneStockManager.DAL
{
    public static class LocalDataAccessLayer
    {
        public static MainViewModel LoadMainViewModel()
        {
            return File.Exists(DataFilePath) ? DeserializeMainViewModel() : MainViewModel.GenerateTemplate();
        }

        public static void SaveMainViewModel(MainViewModel model)
        {
            try
            {
                SerializeMainViewModel(model);
            }
            catch (SerializationException e)
            {
                MessageBox.Show("Serialization problem", "Can't serialize file. Maybe there is problem with access to file.");
            }
        }

        private static string DataFilePath => 
            Path.Combine(DataDirectoryPath, "locals.dat");

        private static string DataDirectoryPath => Path.Combine(Environment.GetFolderPath(
            Environment.SpecialFolder.UserProfile), "StockManager");

        private static void CreateAppDataDirectory() => Directory.CreateDirectory(DataDirectoryPath);

        private static MainViewModel DeserializeMainViewModel()
        {
            MainViewModel loadedModel = null;

            var fs = new FileStream(DataFilePath, FileMode.Open);
            try
            {
                var formatter = new BinaryFormatter();

                // Deserialize the hashtable from the file and 
                // assign the reference to the local variable.
                loadedModel = (MainViewModel) formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            return loadedModel;
        }
        
        private static void SerializeMainViewModel(MainViewModel model)
        {

            CreateAppDataDirectory();

            var fs = new FileStream(DataFilePath, FileMode.Create);
            var formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, model);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        

    }
}