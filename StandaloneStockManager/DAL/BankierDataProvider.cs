﻿using HtmlAgilityPack;
using System;
using System.Globalization;
using System.Windows.Media.Imaging;

namespace StandaloneStockManager.DAL
{


    class BankierDataProvider
    {
        static readonly string mainStockPrefix = "http://www.bankier.pl/inwestowanie/profile/quote.html?symbol=";

        public static BitmapImage IntraDayChar (string stockName)
        {
            var uriString = "http://www.bankier.pl/up/charts/" + stockName + "_intraday.png";
            var uri = new Uri(uriString);
            return new BitmapImage(uri);
        }

        public static string LastStockPrice(string stockName)
        {
            var Url = mainStockPrefix + stockName;
            var web = new HtmlWeb();
            var doc = web.Load(Url);
            var xPathPriceAdress = @"//*[@id=""boxProfilHeader""]/div[1]/div/div[1]";
            return doc.DocumentNode.SelectNodes(xPathPriceAdress)[0].InnerText;
            //string priceM = Decimal.Parse(price, CultureInfo.InvariantCulture);
            //return priceM;
        }

        public static decimal LastDecimalStockPrice(string stockName)
        {
            var stringPrice = LastStockPrice(stockName);
            decimal parseResult;

            Decimal.TryParse(stringPrice, NumberStyles.Currency, CultureInfo.CurrentCulture, out parseResult);
            return parseResult;
        }
    }
}
